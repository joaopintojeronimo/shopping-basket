#!/usr/bin/env node

/* eslint no-console: 0 */  // it's a CLI, must use console

const inquirer = require('inquirer');
const rightPad = require('right-pad');
const leftPad = require('left-pad');
const uniq = require('lodash.uniq');
const products = require('../products');
const checkout = require('../checkout');

let productsToBuy = [];

const formatProduct = product => ({
  name: `${rightPad(product.name, 6)} - £${product.price.toFixed(2)}`
    .concat(product.promotion ? ` - (${product.promotion})` : ''),
  value: product.name,
});

function displayBasketSummary(basket) {
  console.log('You are checking out with');
  uniq(basket.products).forEach((product) => {
    const count = basket.products.filter(p => p.name === product.name).length;
    console.log(` - ${count} x ${product.name}`);
  });
  console.log();
  console.log(leftPad('Total Price: ', 15), basket.totalPrice.toFixed(2));
  if (basket.promotionsApplied.length > 0) {
    console.log(leftPad('Promotions: ', 15), basket.promotionsApplied.join(', '));
  }
  console.log(leftPad('Discount: ', 15), basket.discountedValue.toFixed(2));
  console.log('----------------------');
  console.log(leftPad('Final Price: ', 15), basket.finalPrice.toFixed(2));
}

console.log('Welcome to the shop! \n');

function ask() {
  inquirer.prompt([
    {
      type: 'list',
      name: 'product',
      message: 'Choose a product',
      choices: products.map(formatProduct).concat([
        new inquirer.Separator(),
        {
          name: 'That\'s all, I\'d like to pay now',
          value: 'checkout',
        },
      ]),
    },
  ]).then((answer) => {
    if (answer.product === 'checkout') { // Customer finished shopping
      displayBasketSummary(checkout(productsToBuy));
      return;
    }

    // I could do just a basket.push but the functional gods will kill me
    // because of mutability
    const chosenProduct = products
      .filter(product => product.name === answer.product)[0];
    productsToBuy = productsToBuy.concat([chosenProduct]);

    ask(); // asking again what the customer wants
  }).catch((err) => {
    console.log('oops, does this mean I didn\'t pass the interview ?');
    console.error(err);
    process.exit(-1);
  });
}

ask();
