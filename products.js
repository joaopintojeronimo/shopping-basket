const Decimal = require('decimal.js');

module.exports = [
  {
    name: 'Butter',
    price: new Decimal(0.8),
    promotion: 'Buy 2 Butter and get a Bread at 50% off',
  },
  {
    name: 'Milk',
    price: new Decimal(1.15),
    promotion: 'Buy 3 Milk and get the 4th milk for free',
  },
  {
    name: 'Bread',
    price: new Decimal(1),
  },
];
